Bem vindo ao jogo Mastermind!
O jogo consiste em adivinhar uma senha que será gerada a cada partida, composta por quatro cores dentre as citadas: amarelo, verde, azul, roxo, vermelho, laranja.

Inicialmente será necessário inserir quantas tentativas você deseja ter para acertar a senha.
Existem 10 pinos de cada cor no total para serem usados, não podendo exceder esse número por pino na partida.
A cada tentativa será informado por pinos brancos se a cor está na posição certa, por pinos pretos se a cor está na presente na senha porém na posição errada e pela palavra 'vazio' se a cor não está contida na senha.
Nas suas tentativas insira 4 das cores citadas separadas por vírgula para tentar adivinhar a senha, sem repetir cor na mesma tentativa (exemplo: amarelo,verde,azul,roxo).

Boa sorte!

